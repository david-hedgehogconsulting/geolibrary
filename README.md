# Geo Library Project #

A Kotlin geo-spatial library providing location, area and trajectory functionality to be used for 
land, air and marine navigation.

The Geo-entity classes are immutable with methods implemented as queries returning new objects 
rather than modifying object state.


### Setup ###

Use the Android Studio/InteliJ Idea `VCS->Get from Version Control` option to import this gradle 
project and run unit tests by selecting the `Run ...Tests` menu option within the IDE or on the 
command line by running the command `./gradlew test` on Mac/Linux or `gradlew test` on Windows 


### Reference ###

The library provides the following classes:

#### Latitude ####
Degrees of Latitude

Exposes it's value as **degrees**

Inputs limited to the range -90..90. Inputs outside this range will cause an exception
during instantiation


#### Longitude ####
Degrees of Longitude

Exposes it's value as **degrees**

Inputs outside the range -180..180(right exclusive) wrapped inside the range 


#### GeoPoint ###
Represents any location on (or above) Earth

Can be constructed using **Latitude** and **Longitude** objects or primitive latitude and longitude 
values in degrees along with optional elevation above sea level in metres (defaulting to 0.0)

Exposes a function **translate(vector)** and **+** operator to allow applying translations 
using **GeoVector** 


#### GeoLine ####
Represents a straight line between 2 Points

Constructed using 2 **GeoPoint** objects representing start and end locations 
plus an optional **GeoCalcs** object used to perform the geo-spatial calculations

Exposes a field **lengthMetres** returning the length of the line

Exposes a function **getPointAtDistance(distanceMetres)** which returns a **GeoPoint** 
corresponding to the location along the line for the given distance or **null** if the distance 
extends beyond the boundaries of the line


#### GeoPolyLine ####
Represents a segmented line between 2 or more Points

Constructed using a list of **GeoPoint** at least 2 objects plus an optional **GeoCalcs** object used to 
perform the geo-spatial calculations

Exposes a field **lines** returning the list of **GeoLine** segments corresponding to the given points

Exposes a function **getPointAtDistance(distanceMetres)** which returns a **GeoPoint** 
corresponding to the location along the line segments for the given distance or **null** if the distance 
extends beyond the boundaries of the line segments

Exposes a function **translate(vector)** which applies the given **GeoVector** and returns a 
translated **GeoPloyLine** object


#### GeoRectangle ####
Represents a rectangular area, defined by 2 corner Points

Constructed using 2 **GeoPoint** objects representing South/West and North/East corners

Exposes a function **containsPoint(point)** which returns true if the given point is contained 
within the rectangle (ignoring elevation) or false if the point lies outside the rectangle.

Exposes a function **translate(vector)** which applies the given **GeoVector** and returns a 
translated **GeoRectangle** object
 
 
#### GeoVector ####
Represents a 3D shift for translation of other 3D objects
Constructed using primitive latitude (degrees), longitude (degrees) and elevation (metres) values


#### GeoCalcs ####
An interface allowing clients to develop their own implementations of the geo-spatial 
calculations utilised by the above classes

Exposes a function **calcLengthInMetres(line)** where implementations should return distance in 
metres corresponding to the given **GeoLine**

Exposes a function **calcPointOnLine(line, distanceMetres)** where implementations should return 
a **GeoPoint** corresponding to the given **GeoLine** and distance 


#### HaversineCalcs ####
Default implementation of the **GeoCalcs** interface using calculations based on the Haversine 
formula
