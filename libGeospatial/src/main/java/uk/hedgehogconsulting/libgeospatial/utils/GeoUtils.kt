package uk.hedgehogconsulting.libgeospatial.utils

/**
 * GeoUtils
 * Utility constants, types and functions
 */

const val FINE_DELTA = 0.0005
const val COARSE_DELTA = 1.0

typealias Degrees = Double
typealias Radians = Double

fun Degrees.toRadians(): Radians = this / 180 * Math.PI
fun Radians.toDegrees(): Degrees = this * 180 / Math.PI
