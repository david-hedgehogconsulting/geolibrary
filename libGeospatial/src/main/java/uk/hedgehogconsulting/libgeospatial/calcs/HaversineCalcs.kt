package uk.hedgehogconsulting.libgeospatial.calcs

import uk.hedgehogconsulting.libgeospatial.models.GeoLine
import uk.hedgehogconsulting.libgeospatial.models.GeoPoint
import uk.hedgehogconsulting.libgeospatial.models.Latitude
import uk.hedgehogconsulting.libgeospatial.models.Longitude
import uk.hedgehogconsulting.libgeospatial.utils.Degrees
import uk.hedgehogconsulting.libgeospatial.utils.Radians
import uk.hedgehogconsulting.libgeospatial.utils.toDegrees
import uk.hedgehogconsulting.libgeospatial.utils.toRadians
import kotlin.math.*

/**
 * Default calculation implementation utilising Haversine formula
 */
class HaversineCalcs : GeoCalcs {

    override fun calcLengthInMetres(line: GeoLine): Double = line.getLengthInMetres()

    override fun calcPointOnLine(line: GeoLine, distanceMetres: Double): GeoPoint =
        getPointAtDistanceOnBearing(
            line.startPoint,
            line.getBearingRadians(),
            line.getElevationRadians(),
            distanceMetres
        )

    private fun getPointAtDistanceOnBearing(
        point: GeoPoint,
        bearingRadians: Radians,
        elevationRadians: Radians,
        distanceMetres: Double
    ): GeoPoint {
        val latRads = point.lat.radians
        val lngRads = point.lng.radians

        val flatDistanceInMetres = cos(elevationRadians) * distanceMetres

        val latDest = asin(
            sin(latRads) * cos(flatDistanceInMetres / EARTH_RADIUS_METRES) +
                    cos(latRads) * sin(flatDistanceInMetres / EARTH_RADIUS_METRES) * cos(
                bearingRadians
            )
        )
        val lngDest = lngRads + atan2(
            sin(bearingRadians) * sin(flatDistanceInMetres / EARTH_RADIUS_METRES) * cos(latRads),
            cos(flatDistanceInMetres / EARTH_RADIUS_METRES) - sin(latRads) * sin(latDest)
        )

        val elevationMetres = sin(elevationRadians) * distanceMetres

        return GeoPoint(
            latDest.toDegrees(),
            lngDest.toDegrees(),
            elevationMetres
        )
    }

    private fun GeoLine.getBearingRadians(): Degrees {
        val lat1 = startPoint.lat.radians
        val lat2 = endPoint.lat.radians
        val deltaLng = endPoint.lng.radians - startPoint.lng.radians
        val y = sin(deltaLng) * cos(lat2)
        val x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(deltaLng)
        return atan2(y, x)
    }

    private fun GeoLine.getElevationRadians(): Radians =
        asin(getElevationDelta() / lengthMetres)

    private fun GeoLine.getLengthInMetres(): Double =
        getFlatDistance().adjustDistanceForElevationDelta(getElevationDelta())

    private fun GeoLine.getFlatDistance(): Double {
        val lat1 = startPoint.lat.radians
        val lat2 = endPoint.lat.radians
        val lng1 = startPoint.lng.radians
        val lng2 = endPoint.lng.radians
        val deltaLat = lat2 - lat1
        val deltaLng = lng2 - lng1

        val a = sin(deltaLat / 2) * sin(deltaLat / 2) +
                cos(lat1) * cos(lat2) *
                sin(deltaLng / 2) * sin(deltaLng / 2)
        val c = 2.0 * atan2(sqrt(a), sqrt(1 - a))
        return c * EARTH_RADIUS_METRES
    }

    private fun GeoLine.getElevationDelta(): Double =
        endPoint.elevationMetres - startPoint.elevationMetres

    private fun Double.adjustDistanceForElevationDelta(elevationDeltaMetres: Double): Double =
        hypot(this, elevationDeltaMetres)

    private val Latitude.radians: Radians
        get() = degrees.toRadians()

    private val Longitude.radians: Radians
        get() = degrees.toRadians()

    private companion object {
        const val EARTH_RADIUS_METRES = 6371000
    }
}