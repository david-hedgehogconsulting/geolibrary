package uk.hedgehogconsulting.libgeospatial.calcs

import uk.hedgehogconsulting.libgeospatial.models.GeoLine
import uk.hedgehogconsulting.libgeospatial.models.GeoPoint

/**
 * Interface to allow clients to implement their own calculations with higher/lower accuracy
 */
interface GeoCalcs {
    fun calcLengthInMetres(line: GeoLine): Double
    fun calcPointOnLine(line: GeoLine, distanceMetres: Double): GeoPoint
}