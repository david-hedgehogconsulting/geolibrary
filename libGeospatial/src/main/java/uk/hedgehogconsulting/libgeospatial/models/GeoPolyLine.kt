package uk.hedgehogconsulting.libgeospatial.models

import uk.hedgehogconsulting.libgeospatial.calcs.GeoCalcs
import uk.hedgehogconsulting.libgeospatial.calcs.HaversineCalcs

/**
 * GeoPolyLine
 * Represents a segmented line between 2 or more Points
 */
data class GeoPolyLine(val points: List<GeoPoint>, private val calcs: GeoCalcs = HaversineCalcs()) {

    init {
        require(points.size > 1) { "Minimum of 2 points are required to construct a GeoPolyLine" }
    }

    val lines: List<GeoLine> = getLineList()

    fun translate(vector: GeoVector): GeoPolyLine =
        GeoPolyLine(
            points.map { it + vector },
            calcs
        )

    fun getPointAtDistance(distanceMetres: Double): GeoPoint? =
        findPointAtDistance(distanceMetres, lines)

    /**
     * findPointAtDistance()
     * recursively searches for line spanning given distance and queries the target line
     * to retrieve required point
     * returns point if within polyline otherwise returns null
     */
    private fun findPointAtDistance(distanceMetres: Double, lineList: List<GeoLine>): GeoPoint? =
        if (lineList.isEmpty()) {
            null
        } else {
            lineList.first().let { line ->
                line.getPointAtDistance(distanceMetres)
                    ?: findPointAtDistance(distanceMetres - line.lengthMetres, lineList.drop(1))
            }
        }

    /**
     * getLineList()
     * Constructs a list of lines from a list of points
     * preferred implementation would use `scan()` rather than `fold()`
     * but `scan()` is currently an experimental feature so not suitable for
     * production code
     */
    private fun getLineList(): List<GeoLine> =
        if (points.size < 2) {
            emptyList()
        } else {
            points.fold(listOf(EMPTY_LINE)) { accumulator, geoPoint ->
                accumulator + GeoLine(
                    accumulator.last().endPoint,
                    geoPoint,
                    calcs
                )
            }
                .drop(2) // `drop(2)` removes initial empty line and first generated line which has empty start point

        }

    private companion object {
        val EMPTY_LINE = GeoLine(
            GeoPoint(
                0.0,
                0.0
            ), GeoPoint(0.0, 0.0)
        )
    }
}