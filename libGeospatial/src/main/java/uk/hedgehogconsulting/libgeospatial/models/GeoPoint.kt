package uk.hedgehogconsulting.libgeospatial.models

import uk.hedgehogconsulting.libgeospatial.utils.Degrees

data class Latitude(val degrees: Degrees) {
    init {
        require(degrees >= -90 && degrees <= 90) { "Invalid Latitude Value" }
    }

    operator fun plus(otherDegrees: Degrees): Latitude =
        Latitude(degrees + otherDegrees)
}

data class Longitude(private val rawDegrees: Degrees) {
    val degrees: Degrees = wrapWithinRange()

    operator fun plus(otherDegrees: Degrees): Longitude =
        Longitude(degrees + otherDegrees)

    private fun wrapWithinRange(): Degrees =
        (rawDegrees + 180).rem(360).let {
            if (it < 0) {
                it + 180
            } else {
                it - 180
            }
        }
}

/**
 * GeoPoint
 * Represents any location on (or above) Earth
 *
 * Provides secondary constructor to allow construction using primitive values
 */
data class GeoPoint(val lat: Latitude, val lng: Longitude, val elevationMetres: Double = 0.0) {
    constructor(latDegrees: Degrees, lngDegrees: Degrees, elevationMetres: Double = 0.0) : this(
        Latitude(latDegrees),
        Longitude(lngDegrees),
        elevationMetres
    )

    fun translate(vector: GeoVector): GeoPoint =
        GeoPoint(
            lat + vector.degreesNorth,
            lng + vector.degreesEast,
            elevationMetres + vector.elevationMetres
        )

    operator fun plus(vector: GeoVector): GeoPoint = translate(vector)
}
