package uk.hedgehogconsulting.libgeospatial.models

import uk.hedgehogconsulting.libgeospatial.calcs.GeoCalcs
import uk.hedgehogconsulting.libgeospatial.calcs.HaversineCalcs

/**
 * GeoLine
 * Represents a straight line between 2 Points
 */
data class GeoLine(
    val startPoint: GeoPoint,
    val endPoint: GeoPoint,
    private val calcs: GeoCalcs = HaversineCalcs()
) {

    val lengthMetres: Double = calcs.calcLengthInMetres(this)

    fun translate(vector: GeoVector): GeoLine =
        GeoLine(
            startPoint + vector,
            endPoint + vector,
            calcs
        )

    /**
     * getPointAtDistance()
     * returns point if within line otherwise returns null
     */
    fun getPointAtDistance(distanceMetres: Double): GeoPoint? =
        if (distanceMetres in (0.0..lengthMetres)) {
            calcs.calcPointOnLine(this, distanceMetres)
        } else {
            null
        }
}
