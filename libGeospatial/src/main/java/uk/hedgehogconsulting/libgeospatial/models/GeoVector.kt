package uk.hedgehogconsulting.libgeospatial.models

import uk.hedgehogconsulting.libgeospatial.utils.Degrees

/**
 * GeoVector
 * Represents a 3D shift for translation of other 3D objects
 */
data class GeoVector(
    val degreesNorth: Degrees,
    val degreesEast: Degrees,
    val elevationMetres: Double
)