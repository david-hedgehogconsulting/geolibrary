package uk.hedgehogconsulting.libgeospatial.models

/**
 * GeoRectangle
 * Represents a rectangular area, defined by 2 corner Points
 */
data class GeoRectangle(private val southWest: GeoPoint, private val northEast: GeoPoint) {

    fun containsPoint(point: GeoPoint): Boolean =
        when {
            southWest.lng.degrees > northEast.lng.degrees ->
                rectangleSpanningAntiMeridianContains(point)
            else -> point.lat.degrees in (southWest.lat.degrees..northEast.lat.degrees) &&
                    point.lng.degrees in (southWest.lng.degrees..northEast.lng.degrees)
        }

    fun translate(vector: GeoVector): GeoRectangle =
        GeoRectangle(
            southWest + vector,
            northEast + vector
        )

    private fun rectangleSpanningAntiMeridianContains(point: GeoPoint): Boolean {
        val rectangle1 = GeoRectangle(
            southWest,
            GeoPoint(
                northEast.lat.degrees,
                MAX_LONGITUDE
            )
        )
        val rectangle2 = GeoRectangle(
            GeoPoint(
                southWest.lat.degrees,
                MIN_LONGITUDE
            ), northEast
        )
        return rectangle1.containsPoint(point) || rectangle2.containsPoint(point)
    }

    private companion object {
        const val MIN_LONGITUDE = -180.0
        const val MAX_LONGITUDE = 179.999999
        // Scope for error where points have longitude > MAX_LONGITUDE and < 180.0
        // Tried to use 180 - Double.MIN_VALUE but it got rounded to 180 in calcs
        // so leaving this as an open known limitation for now
    }
}