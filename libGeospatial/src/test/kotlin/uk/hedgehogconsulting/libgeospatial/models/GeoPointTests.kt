package uk.hedgehogconsulting.libgeospatial.models

import org.junit.Assert.assertEquals
import org.junit.Test
import uk.hedgehogconsulting.libgeospatial.utils.FINE_DELTA

class GeoPointTests {

    @Test
    fun `valid latitude value throws no exception`() {
        val latitude = Latitude(20.0)
        assertEquals(20.0, latitude.degrees, FINE_DELTA)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `invalid latitude throws exception`() {
        Latitude(200.0)
    }

    @Test
    fun `valid longitude returns given value`() {
        val longitude = Longitude(100.0)
        assertEquals(100.0, longitude.degrees, FINE_DELTA)
    }

    @Test
    fun `invalid longitude wraps given value within range`() {
        val longitude1 = Longitude(270.0)
        assertEquals(-90.0, longitude1.degrees, FINE_DELTA)

        val longitude2 = Longitude(-270.0)
        assertEquals(90.0, longitude2.degrees, FINE_DELTA)

        val longitude3 = Longitude(360.0)
        assertEquals(0.0, longitude3.degrees, FINE_DELTA)
    }

    @Test
    fun `translate point returns expected result`() {
        val vector = GeoVector(1.0, 1.0, 100.0)
        val bingley = GeoPoint(
            53.856389,
            -1.828889,
            0.0
        )

        val translated = bingley.translate(vector)

        assertEquals(54.856389, translated.lat.degrees, FINE_DELTA)
        assertEquals(-0.828889, translated.lng.degrees, FINE_DELTA)
        assertEquals(100.0, translated.elevationMetres, FINE_DELTA)
    }


    @Test
    fun `plus operator adds vector`() {
        val vector = GeoVector(1.0, 1.0, 100.0)
        val bingley = GeoPoint(
            53.856389,
            -1.828889,
            0.0
        )

        val translated = bingley + vector

        assertEquals(54.856389, translated.lat.degrees, FINE_DELTA)
        assertEquals(-0.828889, translated.lng.degrees, FINE_DELTA)
        assertEquals(100.0, translated.elevationMetres, FINE_DELTA)
    }
}