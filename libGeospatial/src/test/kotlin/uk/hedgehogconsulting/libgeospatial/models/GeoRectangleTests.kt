package uk.hedgehogconsulting.libgeospatial.models

import org.junit.Test

import org.junit.Assert.*

class GeoRectangleTests {

    @Test
    fun `containsPoint() returns true if point in rectangle`() {

        val point = GeoPoint(0.0, 0.0)

        val rectangle = GeoRectangle(
            GeoPoint(
                -1.0,
                -1.0
            ), GeoPoint(1.0, 1.0)
        )

        val result = rectangle.containsPoint(point)

        assertTrue(result)
    }

    @Test
    fun `containsPoint() returns false if point outside rectangle`() {

        val point = GeoPoint(2.0, 2.0)

        val rectangle = GeoRectangle(
            GeoPoint(
                -1.0,
                -1.0
            ), GeoPoint(1.0, 1.0)
        )

        val result = rectangle.containsPoint(point)

        assertFalse(result)
    }


    @Test
    fun `containsPoint() returns false if rectangle invalid`() {

        val point = GeoPoint(0.0, 0.0)

        val rectangle = GeoRectangle(
            GeoPoint(
                1.0,
                1.0
            ), GeoPoint(-1.0, 1.0)
        )

        val result = rectangle.containsPoint(point)

        assertFalse(result)
    }

    @Test
    fun `containsPoint() returns expected value when rectangle spans anti-meridian`() {
        val pointWestOfAntiMeridian =
            GeoPoint(-16.0, 179.9)
        val pointEastOfAntiMeridian =
            GeoPoint(-16.0, -179.9)
        val pointOutsideOfRectangle =
            GeoPoint(-16.0, -170.0)

        val rectangle = GeoRectangle(
            GeoPoint(
                -17.0,
                179.0
            ), GeoPoint(-15.0, -179.0)
        )

        val resultWestOfAntiMeridian = rectangle.containsPoint(pointWestOfAntiMeridian)
        val resultEastOfAntiMeridian = rectangle.containsPoint(pointEastOfAntiMeridian)
        val resultOutsideOfRectangle = rectangle.containsPoint(pointOutsideOfRectangle)

        assertTrue(resultWestOfAntiMeridian)
        assertTrue(resultEastOfAntiMeridian)
        assertFalse(resultOutsideOfRectangle)
    }

    @Test
    fun `containsPoint() returns expected value for large rectangle`() {
        val pointInside = GeoPoint(0.0, 0.0)
        val pointOutside = GeoPoint(0.0, 70.0)

        val rectangle = GeoRectangle(GeoPoint(-25.0, -50.0), GeoPoint(75.0, 50.0))

        val resultInside = rectangle.containsPoint(pointInside)
        val resultOutside = rectangle.containsPoint(pointOutside)

        assertTrue(resultInside)
        assertFalse(resultOutside)
    }

    @Test
    fun `translate() performs expected translation`() {

        val rectangle = GeoRectangle(
            GeoPoint(
                -1.0,
                -1.0
            ), GeoPoint(1.0, 1.0)
        )

        val translated = rectangle.translate(
            GeoVector(
                1.0,
                1.0,
                1.0
            )
        )

        assertEquals(
            GeoRectangle(
                GeoPoint(
                    0.0,
                    0.0,
                    1.0
                ),
                GeoPoint(
                    2.0,
                    2.0,
                    1.0
                )
            ),
            translated
        )
    }
}