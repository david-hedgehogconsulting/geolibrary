package uk.hedgehogconsulting.libgeospatial.models

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import uk.hedgehogconsulting.libgeospatial.calcs.HaversineCalcs
import uk.hedgehogconsulting.libgeospatial.utils.FINE_DELTA

class GeoPolyLineTests {

    @Test(expected = IllegalArgumentException::class)
    fun `GeoPolyLine() throws exception if empty list passed`() {
        val emptyPoints = emptyList<GeoPoint>()

        GeoPolyLine(emptyPoints)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GeoPolyLine() throws exception if single point list passed`() {
        val emptyPoints = listOf(GeoPoint(1.0, 1.0))

        GeoPolyLine(emptyPoints)
    }

    @Test
    fun `getLines() returns expected lines`() {

        val points = listOf(
            GeoPoint(-1.0, 1.0),
            GeoPoint(-0.5, 1.5),
            GeoPoint(0.0, 2.0),
            GeoPoint(0.5, 2.5)
        )
        val calcs = HaversineCalcs()
        val polyLine = GeoPolyLine(points, calcs)

        val lines = polyLine.lines

        assertEquals(
            listOf(
                GeoLine(
                    GeoPoint(-1.0, 1.0),
                    GeoPoint(-0.5, 1.5),
                    calcs
                ),
                GeoLine(
                    GeoPoint(-0.5, 1.5),
                    GeoPoint(0.0, 2.0),
                    calcs
                ),
                GeoLine(
                    GeoPoint(0.0, 2.0),
                    GeoPoint(0.5, 2.5),
                    calcs
                )
            ),
            lines
        )
    }

    @Test
    fun `translate performs expected translation`() {
        val points = listOf(
            GeoPoint(-1.0, 1.0, 0.0),
            GeoPoint(-0.5, 1.5, 0.5),
            GeoPoint(0.0, 2.0, 1.0),
            GeoPoint(0.5, 2.5, 1.5)
        )
        val polyLine = GeoPolyLine(points)

        val translated = polyLine.translate(
            GeoVector(
                1.0,
                1.0,
                1.0
            )
        )

        assertEquals(
            listOf(
                GeoPoint(
                    0.0,
                    2.0,
                    1.0
                ),
                GeoPoint(
                    0.5,
                    2.5,
                    1.5
                ),
                GeoPoint(
                    1.0,
                    3.0,
                    2.0
                ),
                GeoPoint(
                    1.5,
                    3.5,
                    2.5
                )
            ),
            translated.points
        )
    }

    @Test
    fun `getPointAtDistance() returns expected point`() {

        val points = listOf(
            GeoPoint(0.0, 0.0),
            GeoPoint(0.0, 0.009),
            GeoPoint(0.0, 0.018),
            GeoPoint(0.0, 0.027)
        )

        val polyLine =
            GeoPolyLine(points)

        val point = polyLine.getPointAtDistance(2500.0)
            ?: throw IllegalArgumentException("getPointAtDistance() returned null")

        assertEquals(0.0, point.lat.degrees, FINE_DELTA)
        assertEquals(0.022, point.lng.degrees, FINE_DELTA)
    }

    @Test
    fun `getPointAtDistance() returns null if distance exceeds length of polyline`() {
        val points = listOf(
            GeoPoint(0.0, 0.0),
            GeoPoint(0.0, 0.009),
            GeoPoint(0.0, 0.018),
            GeoPoint(0.0, 0.027)
        )

        val polyLine = GeoPolyLine(points)

        val point = polyLine.getPointAtDistance(3500.0)

        assertNull(point)
    }

}