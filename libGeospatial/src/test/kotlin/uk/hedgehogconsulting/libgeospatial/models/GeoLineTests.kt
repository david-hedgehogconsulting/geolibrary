package uk.hedgehogconsulting.libgeospatial.models

import org.junit.Assert.assertEquals
import org.junit.Test
import uk.hedgehogconsulting.libgeospatial.utils.COARSE_DELTA
import uk.hedgehogconsulting.libgeospatial.utils.FINE_DELTA

class GeoLineTests {

    @Test
    fun `lengthMetres returns correct distance for points with same elevation`() {
        val greenwich = GeoPoint(
            51.4934,
            0.0098
        )
        val bingley = GeoPoint(
            53.856389,
            -1.828889
        )

        val line = GeoLine(
            greenwich,
            bingley
        )
        val distance = line.lengthMetres

        assertEquals(290503.0, distance, COARSE_DELTA)
    }

    @Test
    fun `lengthMetres returns 0 distance for identical points`() {
        val bingley = GeoPoint(
            53.856389,
            -1.828889
        )

        val line =
            GeoLine(bingley, bingley)

        val distance = line.lengthMetres

        assertEquals(0.0, distance, FINE_DELTA)
    }

    @Test
    fun `lengthMetres returns vertical distance for identical points with different elevations`() {
        val bingley1 = GeoPoint(
            53.856389,
            -1.828889,
            0.0
        )
        val bingley2 = GeoPoint(
            53.856389,
            -1.828889,
            100.0
        )

        val line = GeoLine(
            bingley1,
            bingley2
        )

        val distance = line.lengthMetres

        assertEquals(100.0, distance, FINE_DELTA)
    }

    @Test
    fun `lengthMetres returns correct distance for points with different elevations`() {
        val bingley1 = GeoPoint(
            53.856389,
            -1.828889,
            0.0
        )
        val bingley2 = GeoPoint(
            53.848711,
            -1.838549,
            -200.0
        )

        val line = GeoLine(
            bingley1,
            bingley2
        )

        val distance = line.lengthMetres

        assertEquals(1082.0, distance, COARSE_DELTA)
    }

    @Test
    fun `lengthMetres returns same distance regardless of elevation sign`() {
        val bingley1 = GeoPoint(
            53.856389,
            -1.828889,
            0.0
        )
        val bingley2 = GeoPoint(
            53.848711,
            -1.838549,
            200.0
        )
        val bingley3 = GeoPoint(
            53.848711,
            -1.838549,
            -200.0
        )

        val line1 = GeoLine(
            bingley1,
            bingley2
        )
        val line2 = GeoLine(
            bingley1,
            bingley3
        )

        val distance1 = line1.lengthMetres
        val distance2 = line2.lengthMetres

        assertEquals(distance1, distance2, FINE_DELTA)
    }

    @Test
    fun `lengthMetres returns expected distance when line spans anti-meridian`() {
        val point1 = GeoPoint(0.0, 179.9)
        val point2 = GeoPoint(0.0, -179.9)

        val line =
            GeoLine(point1, point2)

        val distance = line.lengthMetres

        assertEquals(distance, 22238.9853, FINE_DELTA)
    }

    @Test
    fun `lengthMetres returns expected distance between poles`() {
        val northPole = GeoPoint(90.0, 135.0)
        val southPole = GeoPoint(-90.0, 45.0)

        val line = GeoLine(northPole, southPole)

        val distanceKm = line.lengthMetres / 1000

        assertEquals(20015.0, distanceKm, COARSE_DELTA)
    }

    @Test
    fun `translate() translates correctly for elevation only`() {
        val vector = GeoVector(0.0, 0.0, 100.0)
        val bingley1 = GeoPoint(
            53.856389,
            -1.828889,
            0.0
        )
        val bingley2 = GeoPoint(
            53.848711,
            -1.838549,
            200.0
        )
        val line = GeoLine(
            bingley1,
            bingley2
        )
        val translated = line.translate(vector)

        assertEquals(53.856389, translated.startPoint.lat.degrees, FINE_DELTA)
        assertEquals(-1.828889, translated.startPoint.lng.degrees, FINE_DELTA)
        assertEquals(100.0, translated.startPoint.elevationMetres, FINE_DELTA)
        assertEquals(53.848711, translated.endPoint.lat.degrees, FINE_DELTA)
        assertEquals(-1.838549, translated.endPoint.lng.degrees, FINE_DELTA)
        assertEquals(300.0, translated.endPoint.elevationMetres, FINE_DELTA)
    }

    @Test
    fun `translate() translates correctly`() {
        val vector = GeoVector(1.0, 1.0, 100.0)
        val bingley1 = GeoPoint(
            53.856389,
            -1.828889,
            0.0
        )
        val bingley2 = GeoPoint(
            53.848711,
            -1.838549,
            200.0
        )
        val line = GeoLine(
            bingley1,
            bingley2
        )
        val translated = line.translate(vector)

        assertEquals(54.856389, translated.startPoint.lat.degrees, FINE_DELTA)
        assertEquals(-0.828889, translated.startPoint.lng.degrees, FINE_DELTA)
        assertEquals(100.0, translated.startPoint.elevationMetres, FINE_DELTA)
        assertEquals(54.848711, translated.endPoint.lat.degrees, FINE_DELTA)
        assertEquals(-0.838549, translated.endPoint.lng.degrees, FINE_DELTA)
        assertEquals(300.0, translated.endPoint.elevationMetres, FINE_DELTA)
    }

    @Test
    fun `getPointAtDistance() returns expected point for flat line`() {
        val point1 = GeoPoint(5.0, 5.0)
        val point2 = GeoPoint(5.1, 5.1)

        val line = GeoLine(point1, point2)

        val distance = line.lengthMetres

        val pointAtDistance = line.getPointAtDistance(distance / 2)
            ?: throw IllegalArgumentException("getPointAtDistance() returned null")

        assertEquals(5.05, pointAtDistance.lat.degrees, FINE_DELTA)
        assertEquals(5.05, pointAtDistance.lng.degrees, FINE_DELTA)
    }

    @Test
    fun `getPointAtDistance() returns expected point for vertical distance`() {
        val point1 = GeoPoint(5.0, 5.0)
        val point2 = GeoPoint(5.0, 5.0, 100.0)

        val line = GeoLine(point1, point2)

        val pointAtDistance = line.getPointAtDistance(50.0)

        assertEquals(
            GeoPoint(
                5.0,
                5.0,
                50.0
            ), pointAtDistance
        )
    }
}